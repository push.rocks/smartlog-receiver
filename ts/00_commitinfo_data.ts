/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartlog-receiver',
  version: '2.0.0',
  description: 'a receiver for smartlog-destination-receiver'
}
